# Galactia Chronicles (Working Title)
My ongoing 2D Turn-Based RPG.

## Dependencies
* [Commons][commons]
* [Characters][characters]
* [Dev][dev]
* [GraphViews][graphviews]
* [DialogueGraph][dialoguegraph]
* [Inventory][inventory]
* [OverworldBehaviours][overworldbehaviours]
* [RPG][rpg]
* [TBS][tbs]
* [Unity Scene Reference][scene_reference]
* [TextMeshPro][text_mesh_pro]
[commons]: https://bitbucket.org/Ben-Silver/commons/ "Commons Submodule"
[characters]: https://bitbucket.org/Ben-Silver/characters/ "Characters Submodule"
[dev]: https://bitbucket.org/Ben-Silver/dev/ "Dev Submodule"
[graphviews]: https://bitbucket.org/Ben-Silver/graphviews/ "GraphViews Submodule"
[dialoguegraph]: https://bitbucket.org/Ben-Silver/dialoguegraph/ "DialogueGraph Submodule"
[inventory]: https://bitbucket.org/Ben-Silver/inventory/ "Inventory Submodule"
[overworldbehaviours]: https://bitbucket.org/Ben-Silver/overworldbehaviours/ "OverworldBehaviours Submodule"
[rpg]: https://bitbucket.org/Ben-Silver/rpg/ "RPG Submodule"
[tbs]: https://bitbucket.org/Ben-Silver/tbs/ "TBS Submodule"
[scene_reference]: https://github.com/JohannesMP/unity-scene-reference "Unity Scene Reference"
[text_mesh_pro]: https://docs.unity3d.com/Manual/com.unity.textmeshpro.html "Text Mesh Pro"