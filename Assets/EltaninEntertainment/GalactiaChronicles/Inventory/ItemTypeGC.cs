namespace EltaninEntertainment.GalactiaChronicles.Menus
{
    public enum ItemTypeGC
    {
        Items,
        Medicine,
        Weapons,
        Armour,
        KeyItems
    }
}
