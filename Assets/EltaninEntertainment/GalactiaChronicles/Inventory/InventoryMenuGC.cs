using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.GalactiaChronicles.Menus
{
    using Inventory;

    public class InventoryMenuGC : InventoryMenu<ItemTypeGC>
    {

        [SerializeField] protected Scrollbar scrollbar;
        [SerializeField] private int inventorySpace = 50;

        protected override void UpdateUI()
        {
            base.UpdateUI();

            int rowsCount = itemSlotRows.Count;

            itemSlotRows[0].SetFirstItem();

            //int totalRows = inventorySpace / 6;
            scrollbar.numberOfSteps = rowsCount;//totalRows;
        }

        #region Editor
#if UNITY_EDITOR
        [ContextMenu("Refresh Pocket Names")]
        protected override void RefreshPocketNames()
        {
            base.RefreshPocketNames();
        }
#endif
        #endregion
    }
}
