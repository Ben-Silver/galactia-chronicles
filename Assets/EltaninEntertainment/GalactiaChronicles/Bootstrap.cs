﻿using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.DataManagement
{
    using Commons.DataManagement;
    using Commons.DataManagement.Flags;
    using Characters.Party;
    using Inventory;
    using Localisation;
    using RPG.Movement;
    using RPG.Quests;
    using Settings.DataManagement;

#if USE_ADDRESSABLES
    using System.Threading.Tasks;
    using AssetManagement;
    using Characters.AssetManagers;
    using Inventory.AssetManagement;
#endif

    public static class Bootstrap
    {
        private static int currentCount = 0;
        private static int totalCount = 0;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initiailse()
        {
#if USE_ADDRESSABLES
            RegisterAddressables(() =>
            {
#endif
                Debug.Log("INITIALISING SAVE DATA");

                // Initialise the game manager
                GameManager.Initialise();

                // Register all the DataManagers
                RegisterComponents();

                // Load the game data
                GameManager.Instance.LoadGame();

                // Initialise the localisation manager
                LocalisationManager.Initialise();

                SettingsManager settings = GameManager.Instance?.GetData<SettingsManager>() as SettingsManager;
                SettingsData settingsData = settings.settingsData;
                LocalisationManager.Instance?.SwitchLanguage(settingsData.language);
#if USE_ADDRESSABLES
            });
#endif
        }

    public static void RegisterComponents()
        {
            GameObject.Instantiate(Resources.Load("PF_FirstTimeSetup"));
            GameObject.Instantiate(Resources.Load("PF_AudioManager"));
            GameManager.Instance.Register(new PartyManager("/Metadata/Player/", "Save", ".json"));
            GameManager.Instance.Register(new PositionManager("/Metadata/Player/", "Map", ".json"));
            GameManager.Instance.Register(new InventoryManager("/Metadata/Items/", "Inventory", ".json"));
            GameManager.Instance.Register(new FlagManager("/Metadata/Flags/", "Flags", ".json"));
            GameManager.Instance.Register(new QuestManager("/Metadata/Quests/", "Quests", ".json"));
            GameManager.Instance.Register(new SettingsManager("/Metadata/Settings/", "Settings", ".json"));
        }

#if USE_ADDRESSABLES
        private static async void RegisterAddressables(System.Action _onComplete)
        {
            Debug.Log("INITIALISING ADDRESSABLES");
            if (AddressablesManager.Instance == null)
                AddressablesManager.Initialise();

            totalCount = 9;

            PlayerAssetManager playerManager = new PlayerAssetManager();
            playerManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(playerManager);
            
            PlayerSpriteAssetManager playerSpriteManager = new PlayerSpriteAssetManager();
            playerSpriteManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(playerSpriteManager);

            EnemyAssetManager enemyManager = new EnemyAssetManager();
            enemyManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(enemyManager);

            EnemySpriteAssetManager enemySpriteAssetManager = new EnemySpriteAssetManager();
            enemySpriteAssetManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(enemySpriteAssetManager);

            ElementAssetManager elementManager = new ElementAssetManager();
            elementManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(elementManager);

            SkillAssetManager skillManager = new SkillAssetManager();
            skillManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(skillManager);

            RaceAssetManager raceManager = new RaceAssetManager();
            raceManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(raceManager);

            ClassAssetManager classManager = new ClassAssetManager();
            classManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(classManager);

            ItemAssetManager itemManager = new ItemAssetManager();
            itemManager.OnReady += OnReady;
            AddressablesManager.Instance?.Register(itemManager);

            while (currentCount < totalCount)
                await Task.Yield();

            _onComplete?.Invoke();
        }

        private static void OnReady(AddressableManager _manager)
        {
            _manager.OnReady -= OnReady;
            currentCount++;
        }
#endif
    }
}