using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    public abstract class CutsceneObject : MonoBehaviour
    {
        public abstract IEnumerator BeginCutscene();
    }
}