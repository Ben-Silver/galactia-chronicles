using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    using RPG.Movement;

    public class MoveCharacters : CutsceneObject
    {
        [Header("Characters")]
        [SerializeField] protected List<CharacterController2D> characters;
        [SerializeField] protected string characterId;
        [SerializeField] protected Vector2 direction;
        [SerializeField] protected float duration;

        public override IEnumerator BeginCutscene()
        {
            CharacterController2D character = GetCharacter(characterId);

            yield return MoveCharacter(character, direction, duration);
        }

        protected void ShowCharacter(string _id, bool _value)
        {
            CharacterController2D character = GetCharacter(_id);

            character.gameObject.SetActive(_value);
        }

        protected void MoveCharacter(string _id, Vector2 _direction, float _duration, Action _onComplete = null)
        {
            CharacterController2D character = GetCharacter(_id);

            StartCoroutine(MoveCharacter(character, _direction, _duration, _onComplete));
        }

        protected IEnumerator MoveCharacter(CharacterController2D _character, Vector2 _direction, float _duration, Action _onComplete = null)
        {
            _character.moveInput = _direction;
            yield return new WaitForSeconds(_duration);

            _character.moveInput = Vector2.zero;

            _onComplete?.Invoke();
        }

        protected CharacterController2D GetCharacter(string _id)
        {
            CharacterController2D character = characters.Find((c) =>
            {
                return c.gameObject.name == _id;
            });

            return character;
        }
    }
}