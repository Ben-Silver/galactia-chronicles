using System.Collections;
using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    using DialogueGraph;
    using DialogueGraph.Data;

    public class ShowDialogue : CutsceneObject
    {
        [Header("Dialogue")]
        [SerializeField] protected DialogueContainer dialogue;

        public override IEnumerator BeginCutscene()
        {
            bool isDone = false;
            DialogueManager.Instance?.ShowDialogue(dialogue.GetRootNode(), () =>
            {
                Debug.Log("I'm done");
                isDone = true;
            });

            yield return new WaitUntil(() => isDone);

            yield break;
        }
    }
}