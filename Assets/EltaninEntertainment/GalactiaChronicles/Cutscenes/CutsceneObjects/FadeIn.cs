using System.Collections;

namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    using Commons.Transitions;

    public class FadeIn : CutsceneObject
    {
        public override IEnumerator BeginCutscene()
        {
            yield return TransitionManager.Instance?.BeginTransition(TransitionType.FadeIn);
        }
    }
}