using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    using Commons.DataManagement;
    using Commons.DataManagement.Flags;

    public class Cutscene : MonoBehaviour
    {
        #region Variables

        [Header("Object")]
        [SerializeField] protected List<CutsceneObject> cutscenes;

        [Header("Conditions")]
        [SerializeField] protected EventTrigger eventTrigger;
        [SerializeField] protected EventTriggerType triggerType;
        [SerializeField] protected List<StringFlagData> keys;
        [SerializeField] protected List<FloatFlagData> values;
        [SerializeField] protected List<IntFlagData> integers;
        [SerializeField] protected List<BoolFlagData> switches;

        protected Coroutine cutsceneCrt;

        #endregion

        #region Initialisation

        protected virtual void Awake()
        {
            if (eventTrigger != EventTrigger.Awake)
                return;

            CheckConditions();
        }
        
        protected virtual void OnEnable()
        {
            if (eventTrigger != EventTrigger.OnEnable)
                return;

            CheckConditions();
        }

        protected virtual void Start()
        {
            if (eventTrigger != EventTrigger.Start)
                return;

            CheckConditions();
        }

        #endregion

        #region Cutscenes

        protected void BeginCutscene()
        {
            ClearThread();
            cutsceneCrt = StartCoroutine(BeginCutsceneCrt());
        }

        protected virtual IEnumerator BeginCutsceneCrt()
        {
            foreach (CutsceneObject cutscene in cutscenes)
            {
                cutscene.gameObject.SetActive(true);
                yield return cutscene.BeginCutscene();
                
                cutscene.gameObject.SetActive(false);
            }

            FlagManager fm = GameManager.Instance?.GetData<FlagManager>() as FlagManager;

            foreach (BoolFlagData swtch in switches)
            {
                string id = swtch.GetId();
                fm.SetSwitch(id, !swtch.GetValue());
            }
        }

        protected void ClearThread()
        {
            if (cutsceneCrt != null)
            {
                StopCoroutine(cutsceneCrt);
                cutsceneCrt = null;
            }
        }

        #endregion

        #region Condition Checking

        protected void CheckConditions()
        {
            FlagManager fm = GameManager.Instance?.GetData<FlagManager>() as FlagManager;

            switch (triggerType)
            {
                case EventTriggerType.None:
                case EventTriggerType.Key:
                    if (CheckKeys(fm))
                        BeginCutscene();
                    break;
                case EventTriggerType.Value:
                    if (CheckValues(fm))
                        BeginCutscene();
                    break;
                case EventTriggerType.Integer:
                    if (CheckIntegers(fm))
                        BeginCutscene();
                    break;
                case EventTriggerType.Switch:
                    if (CheckSwitches(fm))
                        BeginCutscene();
                    break;
            }
        }

        protected bool CheckKeys(FlagManager _flagManager)
        {
            if (keys == null || keys.Count == 0)
                return false;

            bool allMatch = true;
            foreach (var key in keys)
            {
                var flag = _flagManager.GetKey(key.GetId());
                bool match = (flag != null) && (flag.GetValue() == key.GetValue());

                if (!match)
                {
                    allMatch = false;
                    break;
                }
            }

            return allMatch;
        }

        protected bool CheckValues(FlagManager _flagManager)
        {
            if (values == null || values.Count == 0)
                return false;

            bool allMatch = true;
            foreach (var val in values)
            {
                var flag = _flagManager.GetValue(val.GetId());
                bool match = (flag != null) && (flag.GetValue() == val.GetValue());

                if (!match)
                {
                    allMatch = false;
                    break;
                }
            }

            return allMatch;
        }
        
        protected bool CheckIntegers(FlagManager _flagManager)
        {
            if (integers == null || integers.Count == 0)
                return false;

            bool allMatch = true;
            foreach (var integer in integers)
            {
                var flag = _flagManager.GetInteger(integer.GetId());
                bool match = (flag != null) && (flag.GetValue() == integer.GetValue());

                if (!match)
                {
                    allMatch = false;
                    break;
                }
            }

            return allMatch;
        }
        
        protected bool CheckSwitches(FlagManager _flagManager)
        {
            if (switches.IsNullOrEmpty())
                return false;

            bool allMatch = true;
            foreach (var swit in switches)
            {
                var flag = _flagManager.GetSwitch(swit.GetId());
                bool match = (flag != null) && (flag.GetValue() == swit.GetValue());

                if (!match)
                {
                    allMatch = false;
                    break;
                }
            }

            return allMatch;
        }

        #endregion

        #region Editor
#if UNITY_EDITOR
        private void OnValidate()
        {
            if (triggerType == EventTriggerType.None)
            {
                keys.Clear();
                values.Clear();
                integers.Clear();
                switches.Clear();
            }
            
            if (eventTrigger == EventTrigger.None)
            {
                triggerType = EventTriggerType.None;
                keys.Clear();
                values.Clear();
                integers.Clear();
                switches.Clear();
            }
        }
#endif
        #endregion
    }

    public enum EventTrigger
    {
        None,
        Awake,
        OnEnable,
        Start,
        OnTriggerEnter,
        OnTriggerExit,
        OnCollisionEnter,
        OnCollisionExit
    }

    public enum EventTriggerType
    {
        None,
        Key,
        Value,
        Integer,
        Switch
    }
}