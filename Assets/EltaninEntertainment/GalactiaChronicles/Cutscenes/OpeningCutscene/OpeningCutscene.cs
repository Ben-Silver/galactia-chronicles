namespace EltaninEntertainment.GalactiaChronicles.Cutscenes
{
    using Commons.DataManagement;
    using Commons.DataManagement.Flags;

    public class OpeningCutscene : Cutscene
    {
        protected override void Start()
        {
            FlagManager fm = GameManager.Instance?.GetData<FlagManager>() as FlagManager;
            bool passedIntro = fm.GetSwitch("passed_intro").GetValue();

            if (passedIntro)
                return;

            base.Start();
        }

        /*protected override IEnumerator BeginCutscene()
        {
            DialogueManager.Instance?.ShowDialogue(dialogue[0], () =>
            {
                TransitionManager.Instance?.FadeIn(() =>
                {
                    DialogueManager.Instance?.ShowDialogue(dialogue[1], () =>
                    {
                        Debug.Log("TOO MANY INDENTATIONS");
                    });
                });
            });
            yield return null;

            FlagManager fm = GameManager.Instance?.GetData<FlagManager>() as FlagManager;
            fm.SetSwitch("passed_intro", true);
        }*/
    }
}