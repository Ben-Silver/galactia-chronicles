﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.GalactiaChronicles.Title
{
    public class SkyboxController : MonoBehaviour
    {
        [SerializeField] private Material skyboxMaterial;
        [SerializeField] private float rotation = 180.0f;
        [SerializeField] private float modifier = 0.1f;

        // Start is called before the first frame update
        void Start()
        {
            skyboxMaterial.SetFloat("_Rotation", rotation);
        }

        private void OnDisable()
        {
            skyboxMaterial.SetFloat("_Rotation", 180.0f);
        }

        // Update is called once per frame
        void Update()
        {
            rotation += (Time.deltaTime * modifier);
            skyboxMaterial.SetFloat("_Rotation", rotation);
        }
    }
}