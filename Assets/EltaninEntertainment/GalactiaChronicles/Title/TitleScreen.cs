﻿using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.GalactiaChronicles.Title
{
    using Commons.Audio;
    using Commons.DataManagement;
    using Commons.DataManagement.Flags;
    using Commons.SceneControllers;
    using Commons.Transitions;
    using RPG.Movement;
    using UI.Menus;

    public class TitleScreen : MonoBehaviour
    {
        //[SerializeField] private string sceneName = "";
        [SerializeField] private SceneReference scene;

        [SerializeField] private AudioClip music;

        public Button startButton;

        private void Start()
        {
            AudioManager.Instance?.PostEvent(music, EventAction.Play, AudioType.Music);

            PositionManager pm = GameManager.Instance?.GetData<PositionManager>() as PositionManager;

            scene = SceneLoader.GetSceneFromPath(pm.GetScene());
            //sceneName = pm.storedData.map;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
                StartScene();
        }

        public void StartScene()
        {
            TransitionManager.Instance?.FadeOut(() =>
            {
                AudioManager.Instance?.PostEvent(music, EventAction.Stop, AudioType.Music);

                FlagManager fm = GameManager.Instance?.GetData<FlagManager>() as FlagManager;

                bool fade = fm.GetSwitch("passed_intro").GetValue();
                SceneLoader.LoadScene(scene, true);

                PauseMenu.Instance?
                         .ToggleMenuButton(true);
            });
        }
    }
}